package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type PING struct{}

func (p PING) RegisterRoute(r *gin.Engine) {
	r.GET("/ping", p.Status)
}

func (PING) Status(c *gin.Context) {

	// c.String(http.StatusOK, "Working")
	c.JSON(http.StatusOK, gin.H{"message": "Wroking"})

}
