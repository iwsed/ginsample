package controllers

import (
	"ginsample/models"
	"net/http"

	"github.com/gin-gonic/gin"
)

type ProductController struct{}

func (p ProductController) RegisterRoute(r *gin.Engine) {
	r.GET("/prod", p.ShowProd)

	// r.LoadHTMLFiles("templates/list.html")
	r.GET("/prod/list", p.ShowAll)
}

func (ProductController) ShowProd(c *gin.Context) {

	p := new(models.Product)
	p.GetOne() // 这样就不能获取到数据，因为接口里面有新建了一个

	c.JSON(http.StatusOK, gin.H{"message": "Found it", "core": p.Code, "Name": p.Name})
}

func (ProductController) ShowAll(c *gin.Context) {

	p := models.FindAll()
	// for i, k := range p {
	// 	fmt.Println(i, k.Name)
	// }

	prod := make(map[string][]*models.Product)
	prod["Product"] = p // Product的作用是作为 range中 Product的范围

	c.HTML(http.StatusOK, "list.html", prod)
}
