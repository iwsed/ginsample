package models

import (
	"ginsample/db"

	"github.com/jinzhu/gorm"
)

var dbx *gorm.DB

type Product struct {
	gorm.Model
	Code  string
	Price uint
	Name  string
}

func (Product) TableName() string {
	return "product"
}

func init() {

	dbx = db.NewOne()           // 获取db
	dbx.AutoMigrate(&Product{}) //与product 匹配
}

func (p *Product) GetOne() {

	//dbx := db.NewOne()
	//defer db.Close(dbx)	这里close之后，就把link给关闭了；
	dbx.First(&p, 1)
}

func FindAll() []*Product {

	dbx := db.NewOne()
	//	defer db.Close(dbx)	这里close之后，就把link给关闭了；

	// dbx.AutoMigrate(&Product{})

	p := make([]*Product, 0)

	dbx.Find(&p)

	return p
}
